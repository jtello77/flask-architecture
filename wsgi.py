from Service import create_app

"""
Execute app in production environment
"""

app = create_app('PRODUCTION')