from Service import create_app, db
from flask.cli import with_appcontext
import unittest
import click

app = create_app('TESTING')


@click.command()
@click.argument('configuration', nargs=-1)
@with_appcontext
def main(configuration):
    """
    Execute the app with different configurations
    :param configuration: is the configuration that work the app
    :return: None
    """

    if len(configuration) != 0:
        if configuration[0] == 'test':
            test = unittest.TestLoader().discover('Test')
            unittest.TextTestRunner().run(test)
        elif configuration[0] == 'run':
            app = create_app('DEVELOPMENT')
            app.run(debug=True, port='5000')
        else:
            print("Error! Please enter a parameter test or run")
    else:
        print("Error! Please enter a parameter test or run")


if __name__ == "__main__":
    main()
