import os


class Config:
    SECRET_KEY = os.urandom(128)
    instance_relative_config = True


class Development(Config):
    """
    Has all development configuration, with mysql database of test and
    debug activated
    """

    DEBUG = True
    TESTING = False
    USER_DB = os.environ.get('MYSQL_USER_DEVELOPMENT')
    USER_DB_PASS = os.environ.get('MYSQL_PASSWORD_DEVELOPMENT')
    HOST_DB = os.environ.get('MYSQL_HOST_DEVELOPMENT')
    SQLALCHEMY_DATABASE_URI = 'mysql://{}:{}@{}/DATABASE_NAME'.format(USER_DB, USER_DB_PASS, HOST_DB)
    SQLALCHEMY_TRACK_MODIFICATIONS = True



class Testing(Config):
    """
    Has configuration of unit test in a mock database.
    """

    TESTING = True
    DEBUG = True
    USER_DB = os.environ.get('MYSQL_USER_TEST')
    USER_DB_PASS = os.environ.get('MYSQL_PASSWORD_TEST')
    HOST_DB = os.environ.get('MYSQL_HOST_TEST')
    SQLALCHEMY_DATABASE_URI = 'mysql://{}:{}@{}/DATABASE_NAME'.format(USER_DB, USER_DB_PASS, HOST_DB)
    SQLALCHEMY_TRACK_MODIFICATIONS = True

class Preproduction(Config):
    """
    Has configuration of pre-production environment.
    """

    DEBUG = False
    TESTING = False
    USER_DB = os.environ.get('MYSQL_USER_PREPROD')
    USER_DB_PASS = os.environ.get('MYSQL_PASSWORD_PREPROD')
    HOST_DB = os.environ.get('MYSQL_HOST_PREPROD')
    SQLALCHEMY_DATABASE_URI = 'mysql://{}:{}@{}/DATABASE_NAME'.format(USER_DB, USER_DB_PASS, HOST_DB)
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class Production(Config):
    """
    Has configuration of production environment.
    """

    DEBUG = False
    TESTING = False
    USER_DB = os.environ.get('MYSQL_USER')
    USER_DB_PASS = os.environ.get('MYSQL_PASSWORD')
    HOST_DB = os.environ.get('MYSQL_HOST')
    SQLALCHEMY_DATABASE_URI = 'mysql://{}:{}@{}/DATABASE_NAME'.format(USER_DB, USER_DB_PASS, HOST_DB)
    SQLALCHEMY_TRACK_MODIFICATIONS = False


config = {
    'DEVELOPMENT': Development,
    'TESTING': Testing,
    'PREPRODUCTION': Preproduction,
    'PRODUCTION': Production
}
