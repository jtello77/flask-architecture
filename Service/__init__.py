MODULE_NAME = 'Service'
IMPORT_NAME = __name__

from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from config import config


db = SQLAlchemy()


def create_app(config_name='DEVELOPMENT'):
    """
    Create all flask app, load configuration database and make a context
    :param config_name: configuration name of class configuration
    :return: app: is the context of application
    """

    app = Flask(__name__)
    CORS(app)
    app.config.from_object(config[config_name])

    with app.app_context():
        # Imports
        # Here include new imports from service file
        #db.init_app(app)
        #db.create_all(bind='__all__')
        # Blueprint modules
        # app.register_blueprint(authService)

        # create models tables
        @app.shell_context_processor
        def ctx():
            return {'app': app, 'db': db}

        return app
